package Golf;

import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.FileReader;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by breckinr on 11/10/15.
 */
public class CalculateGolfEV {
    public Map<String,GolfBean> gMap;

    public static void main(String[] args) throws Exception {
        CalculateGolfEV read = new CalculateGolfEV();
        read.readGolf();
    }

    public void readGolf() throws Exception {
        gMap = new ConcurrentHashMap<String,GolfBean>();
        ICsvBeanReader beanReader = null;
        try {
            beanReader = new CsvBeanReader(new FileReader("./dfs/res/golf/Golfers.csv"), CsvPreference.STANDARD_PREFERENCE);
            beanReader.getHeader(true);
            // setting header elements to null means those columns are ignored
            final String[] header = new String[]{"Name", "Win"};
            //How to convert csv columns to FighterBean variables
            final CellProcessor[] processors = new CellProcessor[]{
                    new NotNull(), // Name
                    new ParseOdds(), //chance this golfer will win
            };
            GolfBean golfer;
            while ((golfer = beanReader.read(GolfBean.class, header, processors)) != null) {
                gMap.put(golfer.getName(), golfer);
                if(!(gMap.containsKey(golfer.getName()))) {
                    System.err.println("ERROR: Adding golfer failed");
                }
            }

            // Iterate through fighter map and set EV
            Iterator it = gMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry golfEntry = (Map.Entry)it.next();
                GolfBean golfBean = (GolfBean) golfEntry.getValue();
                golfBean.setEv(calculateEV(golfBean));
                System.out.println(String.format("Golfer data is %s",golfBean));
            }

        } finally {
            if (beanReader != null) {
                beanReader.close();
            }
        }
    }

    private int calculateEV(GolfBean gb){
        int ret = 0;
        ret += gb.getWin() * GolfConstants.PWIN;
        return ret;
    }
}

