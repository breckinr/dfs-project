package Golf;

import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;

public class GolfBean {
    private int ev, salary, rank;
    private String name, category;
    private double win;

    public GolfBean() {

        name = category = "";
        win = 0.0;
        rank = ev = salary = 0;
    }

    @Override
    public String toString() {
        return String.format(
                "GolfBean [name=%s,salary=%d,win=%f,ev=%d]",getName(),getSalary(),getWin(),getEv()
        );
    }

    public static CellProcessor[] getProcessors() {
        final CellProcessor[] processors = new CellProcessor[]{
                //new NotNull(), // Event
                new ParseInt(), // Rank
                new NotNull(), // Name
                new ParseDouble(), // Win chance
                new ParseInt() // salary
        };
        return processors;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getWin() {
        return win;
    }

    public void setWin(double win) {
        this.win = win;
    }

    public int getEv() {
        return ev;
    }

    public void setEv(int ev) {
        this.ev = ev;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
