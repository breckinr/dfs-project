package Golf;

import org.supercsv.cellprocessor.CellProcessorAdaptor;
import org.supercsv.cellprocessor.ift.LongCellProcessor;
import org.supercsv.cellprocessor.ift.StringCellProcessor;
import org.supercsv.exception.SuperCsvCellProcessorException;
import org.supercsv.util.CsvContext;

/**
 * Created by breckinr on 11/10/15.
 */
public class ParseOdds extends CellProcessorAdaptor implements StringCellProcessor {
    public ParseOdds() {
    }

    public ParseOdds(LongCellProcessor next) {
        super(next);
    }

    public Object execute(Object value, CsvContext context) {
        this.validateInputNotNull(value, context);
        Double result;
        if(value instanceof Double) {
            result = (Double)value;
        } else {
            if(!(value instanceof String)) {
                String actualClassName = value.getClass().getName();
                throw new SuperCsvCellProcessorException(String.format("the input value should be of type Integer or String but is of type %s", new Object[]{actualClassName}), context, this);
            }

            try {
                result = Double.valueOf((String)value);
            } catch (NumberFormatException var5) {
                throw new SuperCsvCellProcessorException(String.format("\'%s\' could not be parsed as an Integer", new Object[]{value}), context, this, var5);
            }
        }
        double doubleResult = convertOddsToPercentage(result);
        return this.next.execute(doubleResult, context);
    }
    private double convertOddsToPercentage(double i){
        double in = (double)i;
        double ret = 0;
        ret = 1/in;

        return ret;
    }

}
