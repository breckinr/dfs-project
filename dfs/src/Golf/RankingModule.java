package Golf;

import MMA.Tuple;
import Module.Module;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by breckinr on 12/11/2015.
 */
public class RankingModule implements Module{
        String csvPath;
        int weight = 1;
        public Map<String,GolfBean> gMap;

        public RankingModule(String csvPath, int weight){
            this.csvPath = csvPath;
            this.weight = weight;
        }


        public void readGolf() throws Exception {
            gMap = new ConcurrentHashMap<String,GolfBean>();
            ICsvBeanReader beanReader = null;
            try {
                beanReader = new CsvBeanReader(new FileReader(csvPath), CsvPreference.STANDARD_PREFERENCE);
                beanReader.getHeader(true);
                // setting header elements to null means those columns are ignored
                final String[] header = new String[]{"Rank","Name", "Win","Salary"};
                //How to convert csv columns to FighterBean variables
                final CellProcessor[] processors = new CellProcessor[]{
                        new org.supercsv.cellprocessor.ParseInt(), //Rank
                        new NotNull(), // Name
                        new ParseOdds(), //chance this golfer will win
                        new org.supercsv.cellprocessor.ParseInt(), //Salary
                };
                GolfBean golfer;
                while ((golfer = beanReader.read(GolfBean.class, header, processors)) != null) {
                    gMap.put(golfer.getName(), golfer);
                    if(!(gMap.containsKey(golfer.getName()))) {
                        System.err.println("ERROR: Adding golfer failed");
                    }
                }

                // Iterate through fighter map and set EV
                Iterator it = gMap.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry golfEntry = (Map.Entry)it.next();
                    GolfBean golfBean = (GolfBean) golfEntry.getValue();
                    golfBean.setEv(calculateEV(golfBean));
                    System.out.println(String.format("Golfer data is %s",golfBean));
                }

            } finally {
                if (beanReader != null) {
                    beanReader.close();
                }
            }
        }

        private int calculateEV(GolfBean gb){
            int ret = 0;
            int rank = gb.getRank();
            double pointPercentage = (((double)300 - rank)/300);
            ret += (pointPercentage * 100 * weight);
            if(ret <0) ret = 0;
            return ret;
        }


        //called on first module processed, returns a new Map
        public Map<String, Tuple<Integer, Integer>> getResults() {
            try {
                readGolf();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Map<String, Tuple<Integer, Integer>> ret = new HashMap<>();
            //System.out.println(fMap.size());
            Iterator it = gMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry golfEntry = (Map.Entry)it.next();
                GolfBean golfBean = (GolfBean) golfEntry.getValue();
                //System.out.println(String.format("ret data is %s",fighterBean));
                ret.put(golfBean.getName(),  new Tuple<>(golfBean.getEv(),golfBean.getSalary()));
            }

            return ret;
        }

        //called on all modules processed after first, modifies the map passed in
        public void getResults(Map<String, Tuple<Integer, Integer>> oldMap){
            Map<String, Tuple<Integer, Integer>> newmap = getResults();
            // Iterate through fighter map and set EV
            Iterator it = newmap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry golfEntry = (Map.Entry)it.next();
                if(oldMap.containsKey(golfEntry.getKey())){
                    oldMap.get(golfEntry.getKey()).x += gMap.get(golfEntry.getKey()).getEv();
                } else {
                    System.err.println("Mismatching Module error (Golfers don't match)");
                }
            }
        }




    }

