package MMA;

/**
 * Created by breckinr on 9/25/2015.
 */
public class Constants {
    static final int PWR1 = 100;
    static final int PWR2 = 80;
    static final int PWR3 = 65;
    static final int PWD = 50;
    static final int PLR1 = 5;
    static final int PLR2 = 15;
    static final int PLR3 = 25;
    static final int PLD = 30;
}
