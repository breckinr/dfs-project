package MMA;

import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.constraint.UniqueHashCode;
import org.supercsv.cellprocessor.ift.CellProcessor;

/**
 * Created by breckinr on 9/25/2015.
 */
public class FighterBean{
    private int salary,ev;
    private String name, gameInfo, teamAbbrev, opponent;
    private double wr1,wr2,wr3,wbd;

    @Override
    public String toString() {
        return String.format(
                "FighterBean [name=%s,salary=%d,opponent=%s,1=%f,2=%f,3=%f,D=%f,EV=%d]",getName(),getSalary(),getOpponent(),getWr1(),getWr2(),getWr3(),getWbd(),getEv()
        );
    }
    private static CellProcessor[] getProcessors() {
        final CellProcessor[] processors = new CellProcessor[]{
                new NotNull(), // Name
                new ParseInt(), // Salary (must be int)
                new NotNull(), // Opponent
                new NotNull(),//GameInfo
                new UniqueHashCode(),//teamAbbrev
                new ParseMoneyline(), //WR1: chance this fighter will win in rd 1
                new ParseMoneyline(), //WR2: chance this fighter will win in rd 2
                new ParseMoneyline(), //WR3: chance this fighter will win in rd 3
                new ParseMoneyline(), //WBD: chance this fighter will win by decision
        };
        return processors;
    }
    public FighterBean() {
        salary = ev = 0;
        name = gameInfo = teamAbbrev = opponent = "";
        wr1 = wr2 = wr3 = wbd = 0.0;
    }

    public FighterBean(FighterBean rhs) {
        //System.out.println("Copy cons: " + rhs.getTeamAbbrev());
        salary = rhs.getSalary();
        ev = rhs.getEv();
        name = rhs.getName();
        gameInfo = rhs.getGameInfo();
        teamAbbrev = rhs.getTeamAbbrev();
        opponent = rhs.getOpponent();
        wr1 = rhs.getWr1();
        wr2 = rhs.getWr2();
        wr3 = rhs.getWr3();
        wbd = rhs.getWbd();
    }

    public int getSalary() {
        return salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getEv() {
        return ev;
    }

    public void setEv(int ev) {
        this.ev = ev;
    }

    public String getGameInfo() {
        return gameInfo;
    }

    public void setGameInfo(String gameInfo) {
        this.gameInfo = gameInfo;
    }

    public String getTeamAbbrev() {
        return teamAbbrev;
    }

    public void setTeamAbbrev(String teamAbbrev) {
        this.teamAbbrev = teamAbbrev;
    }

    public double getWbd() {
        return wbd;
    }

    public void setWbd(double wbd) {
        this.wbd = wbd;
    }

    public double getWr3() {
        return wr3;
    }

    public void setWr3(double wr3) {
        this.wr3 = wr3;
    }

    public double getWr2() {
        return wr2;
    }

    public void setWr2(double wr2) {
        this.wr2 = wr2;
    }

    public double getWr1() {
        return wr1;
    }

    public void setWr1(double wr1) {
        this.wr1 = wr1;
    }

    public String getOpponent() {
        return opponent;
    }

    public void setOpponent(String opponent) {
        this.opponent = opponent;
    }
}
