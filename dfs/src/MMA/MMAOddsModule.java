package MMA;

import Module.Module;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.constraint.UniqueHashCode;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by breckinr on 12/3/15.
 */
public class MMAOddsModule  implements Module {
    String csvPath;
    public static Map<String,FighterBean> fMap;

    public MMAOddsModule(String csvPath) {
        this.csvPath = csvPath;
        fMap = new ConcurrentHashMap<String,FighterBean>();
    }

    //called on first module processed, returns a new Map
    public Map<String, Tuple<Integer, Integer>> getResults() {
        try {
            readMMA();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, Tuple<Integer, Integer>> ret = new HashMap<>();
        //System.out.println(fMap.size());
        Iterator it = fMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry fighterEntry = (Map.Entry)it.next();
            FighterBean fighterBean = (FighterBean) fighterEntry.getValue();
            System.out.println(String.format("ret data is %s",fighterBean));
            ret.put(fighterBean.getName(),  new Tuple<>(fighterBean.getEv(),fighterBean.getSalary()));
        }

        return ret;
    }

    //called on all modules processed after first, modifies the map passed in
    public void getResults(Map<String, Tuple<Integer, Integer>> oldMap){

    }

    public void readMMA() throws Exception {
        ICsvBeanReader beanReader = null;
        try {
            beanReader = new CsvBeanReader(new FileReader(csvPath), CsvPreference.STANDARD_PREFERENCE);
            beanReader.getHeader(true);
            // setting header elements to null means those columns are ignored
            final String[] header = new String[]{"Name", "Salary", "Opponent", null, null, "WR1","WR2","WR3","WBD"};
            //How to convert csv columns to FighterBean variables
            final CellProcessor[] processors = new CellProcessor[]{
                    new NotNull(), // Name
                    new ParseInt(), // Salary (must be int)
                    new NotNull(), // Opponent
                    new NotNull(),//GameInfo
                    new UniqueHashCode(),//teamAbbrev
                    new ParseMoneyline(), //WR1: chance this fighter will win in rd 1
                    new ParseMoneyline(), //WR2: chance this fighter will win in rd 2
                    new ParseMoneyline(), //WR3: chance this fighter will win in rd 3
                    new ParseMoneyline(), //WBD: chance this fighter will win by decision
            };
            FighterBean fighter;
            while ((fighter = beanReader.read(FighterBean.class, header, processors)) != null) {
                fMap.put(fighter.getName(),fighter);
                if(!(fMap.containsKey(fighter.getName()))) {
                    System.err.println("ERROR: Adding fighter failed");
                }
            }

            // Iterate through fighter map and set EV
            Iterator it = fMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry fighterEntry = (Map.Entry)it.next();
                FighterBean fighterBean = (FighterBean) fighterEntry.getValue();
                FighterBean opponentBean = null;
                if(fMap.containsKey(fighterBean.getOpponent())) {
                    opponentBean = fMap.get(fighterBean.getOpponent());
                }
                else {
                    System.err.println(String.format("ERROR: OPPONENT %s NOT FOUND",fighterBean.getOpponent()));
                }
                fighterBean.setEv(calculateEV(fighterBean,opponentBean));
                //System.out.println(String.format("Fighter data is %s",fighterBean));
            }

        } finally {
            if (beanReader != null) {
                beanReader.close();
            }
        }
        //System.out.println(fMap.toString());
    }

    private int calculateEV(FighterBean fb, FighterBean opp){
        int ret = 0;
        ret += fb.getWr1() * Constants.PWR1;
        ret += fb.getWr2() * Constants.PWR2;
        ret += fb.getWr3() * Constants.PWR3;
        ret += fb.getWbd() * Constants.PWD;
        ret += opp.getWr1() * Constants.PLR1;
        ret += opp.getWr2() * Constants.PLR2;
        ret += opp.getWr3() * Constants.PLR3;
        ret += opp.getWbd() * Constants.PLD;
        return ret;
    }
}
