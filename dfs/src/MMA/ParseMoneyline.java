package MMA;

import org.supercsv.cellprocessor.CellProcessorAdaptor;
import org.supercsv.cellprocessor.ift.LongCellProcessor;
import org.supercsv.cellprocessor.ift.StringCellProcessor;
import org.supercsv.exception.SuperCsvCellProcessorException;
import org.supercsv.util.CsvContext;

import java.util.DoubleSummaryStatistics;

/**
 * Created by breckinr on 9/25/2015.
 */
public class ParseMoneyline extends CellProcessorAdaptor implements StringCellProcessor {
    public ParseMoneyline() {
    }

    public ParseMoneyline(LongCellProcessor next) {
        super(next);
    }

    public Object execute(Object value, CsvContext context) {
        this.validateInputNotNull(value, context);
        Double result;
        if(value instanceof Double) {
            result = (Double)value;
        } else {
            if(!(value instanceof String)) {
                String actualClassName = value.getClass().getName();
                throw new SuperCsvCellProcessorException(String.format("the input value should be of type Integer or String but is of type %s", new Object[]{actualClassName}), context, this);
            }

            try {
                result = Double.valueOf((String)value);
            } catch (NumberFormatException var5) {
                throw new SuperCsvCellProcessorException(String.format("\'%s\' could not be parsed as an Integer", new Object[]{value}), context, this, var5);
            }
        }
        double doubleResult = convertMoneylineToPercentage(result.intValue());
        return this.next.execute(doubleResult, context);
    }
    private double convertMoneylineToPercentage(int i){
        double in = (double)i;
        double ret = 0;
        if(i < 0) {
            ret = (in/(in-100));
        }
        else {
            ret = (100/(in+100));
        }

        return ret;
    }

}
