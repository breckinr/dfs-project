package MMA;

/**
 * Created by Ryan on 11/5/2015.
 */
public class Tuple<X, Y> {
    public X x;
    public final Y y;
    public Tuple(X l, Y v) {
        x = l;
        y = v;
    }
}
