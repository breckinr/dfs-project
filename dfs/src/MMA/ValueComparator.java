package MMA;

import java.util.Comparator;
/**
 * Created by Ryan on 11/9/2015.
 */
public class ValueComparator implements Comparator<Tuple<String[], Integer>> {
    @Override
    public int compare(Tuple<String[], Integer> x, Tuple<String[], Integer> y) {
        return y.y - x.y;

    }
}
