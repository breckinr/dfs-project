package Main;

import MMA.MMAOddsModule;
import MMA.Tuple;
import Module.ModuleProcessor;
import javafx.application.Application;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import scrape.MainScrape;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Jonathan on 9/15/2015.
 */


    public class GUI extends Application implements EventHandler<ActionEvent> {
    Button run;
    Button cancel;
    ToggleButton mod1 = new ToggleButton("module1");
    ToggleButton mod2 = new ToggleButton("module2");
    ToggleButton mod3 = new ToggleButton("module3");
    ToggleButton mod4 = new ToggleButton("module4");
    ToggleButton mod5 = new ToggleButton("module5");
    ToggleButton mod6 = new ToggleButton("module6");
    ToggleButton lineup1 = new ToggleButton("1");
    ToggleButton lineup2 = new ToggleButton("5");
    ToggleButton lineup3 = new ToggleButton("10");
    ToggleButton lineup4 = new ToggleButton("15");
    ToggleButton lineup5 = new ToggleButton("20");
    ToggleButton lineup6 = new ToggleButton("25");
    ToggleGroup lineup_group = new ToggleGroup();
    public static boolean mod1_bool = false;
    public static boolean mod2_bool = false;
    public static boolean mod3_bool = false;
    public static boolean mod4_bool = false;
    public static boolean mod5_bool = false;
    public static boolean mod6_bool = false;
    String output = "Lineups:" + "\n";
    String plyr_incl_str;
    String plyr_excl_str;
    String url_text_string;
    Integer mod1_weight;
    TextField weight;
    TextField url_text;
    TextField plyr_incl_text;
    TextField plyr_exclude_text;
    Integer number_of_lineups = 1;
    Integer number_of_modules = 0;
    //public static Module.Module[] modules;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        //Adds images to background
        Image image1 = new Image("https://mmaniacs.files.wordpress.com/mixed-martial-arts-mma-logo.jpg");
        ImageView imv1 = new ImageView();


        imv1.setImage(image1);
        imv1.setFitHeight(350);
        imv1.setFitWidth(350);
        imv1.setPreserveRatio(true);
        imv1.setSmooth(true);
        imv1.setCache(true);


        //Check boxes
        lineup1.setToggleGroup(lineup_group);
        lineup2.setToggleGroup(lineup_group);
        lineup3.setToggleGroup(lineup_group);
        lineup4.setToggleGroup(lineup_group);
        lineup5.setToggleGroup(lineup_group);
        lineup6.setToggleGroup(lineup_group);

        lineup_group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if (newValue == lineup1) {
                    number_of_lineups = 1;
                } else if (newValue == lineup2) {
                    number_of_lineups = 5;
                } else if (newValue == lineup3) {
                    number_of_lineups = 10;
                } else if (newValue == lineup4) {
                    number_of_lineups = 15;
                } else if (newValue == lineup5) {
                    number_of_lineups = 20;
                } else if (newValue == lineup6) {
                    number_of_lineups = 25;
                }
            }
        });
        Label lineups_label = new Label("Please select the number" + "\n" + "of lineups to create:");
        VBox lineups_box = new VBox();
        lineups_box.getChildren().addAll(lineups_label, lineup1, lineup2, lineup3, lineup4, lineup5, lineup6);
        lineups_box.setAlignment(Pos.CENTER);
        lineups_box.setPadding(new Insets(10,25,10,10));
        lineups_box.setSpacing(10);
        lineups_box.setAlignment(Pos.CENTER);

        //Creates the tab pane and the tabs to be added
        TabPane tp = new TabPane();
        Tab mma_tab = new Tab("MMA");
        Tab golf_tab = new Tab("Golf");


        //Adds listener to Tabs
        tp.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        tp.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (newValue.intValue() == 1) {
                   try{
                       GUI_Golf.display_golf(golf_tab) ;
                   }
                   catch (Exception ex){
                       PopUp.display();
                   }
                }
                if (newValue.intValue() == 2) {
                }
                if (newValue.intValue() == 3) {
                }
                if (newValue.intValue() == 4) {
                }
                if (newValue.intValue() == 5) {
                }
                if (newValue.intValue() == 6) {
                }


            }
        });

        //set the Title and the layout on the tab
        primaryStage.setTitle("FantasyLineup");
        BorderPane grid_buttons = new BorderPane();

        //Text Fields
        GridPane textfields = new GridPane();
        textfields.setAlignment(Pos.TOP_CENTER);
        //textfields.setGridLinesVisible(true);

        Label url_label = new Label("Please Enter the URL: ");
        Label plyr_include = new Label("Enter players to include: ");
        Label plyr_exclude = new Label("Enter players to exclude: ");

        url_text = new TextField();
        url_text.setPromptText("e.g. www.bestfightodds.com");
        url_text.setMinWidth(450);

        plyr_incl_text = new TextField();
        plyr_incl_text.setPromptText("e.g. John Doe, Jane Doe");

        plyr_exclude_text = new TextField();
        plyr_exclude_text.setPromptText("e.g. John Doe, Jane Doe");

        HBox url = new HBox();
        url.setSpacing(10);
        url.getChildren().addAll(url_label, url_text);

        HBox exclude_include = new HBox();
        exclude_include.getChildren().addAll(plyr_exclude, plyr_exclude_text, plyr_include, plyr_incl_text);
        exclude_include.setSpacing(10);

        textfields.setVgap(10);
        textfields.setPadding(new Insets(10, 10, 10, 10));
        textfields.add(exclude_include, 0, 1);
        textfields.add(url, 0, 2);



        //Creates the buttons and sets handler
        run = new Button("Run");
        cancel = new Button("Cancel");

        run.setPrefSize(75, 50);
        cancel.setPrefSize(75, 50);

        run.setOnAction(this);
        cancel.setOnAction(this);

        HBox buttons = new HBox();
        buttons.setSpacing(10);
        buttons.setPadding(new Insets(0, 20, 10, 20));
        buttons.getChildren().addAll(run, cancel);

        buttons.setAlignment(Pos.BOTTOM_RIGHT);

        //Toggle buttons
        GridPane modules = new GridPane();
        Label mod_label = new Label("Please select" + "\n" + " Modules: ");
        Label weight_label = new Label("Please input"+"\n" +" weight: ");
        weight = new TextField();
        weight.setMaxWidth(50);
        weight.setPromptText("1");
        ToggleButton mod1 = new ToggleButton("Expected Value");
        ToggleButton mod2 = new ToggleButton("module2");
        ToggleButton mod3 = new ToggleButton("module3");
        ToggleButton mod4 = new ToggleButton("module4");
        ToggleButton mod5 = new ToggleButton("module5");
        ToggleButton mod6 = new ToggleButton("module6");

        //changes bool when button is selected

        mod1.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (mod1_bool == false) {
                    mod1_bool = true;
                    number_of_modules++;
                } else {
                    mod1_bool = false;
                    number_of_modules--;
                }
            }
        });
        mod2.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (mod2_bool == false) {
                    mod2_bool = true;
                } else {
                    mod2_bool = false;
                }
            }
        });
        mod3.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (mod3_bool == false) {
                    mod3_bool = true;
                } else {
                    mod3_bool = false;
                }
            }
        });
        mod4.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (mod4_bool == false) {
                    mod4_bool = true;
                } else {
                    mod4_bool = false;
                }
            }
        });
        mod5.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (mod5_bool == false) {
                    mod5_bool = true;
                } else {
                    mod5_bool = false;
                }
            }
        });
        mod6.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (mod6_bool == false) {
                    mod6_bool = true;
                } else {
                    mod6_bool = false;
                }
            }
        });

        //sets layout
        modules.setPadding(new Insets(10, 10, 10, 10));
        modules.setVgap(10);
        modules.setHgap(15);
        modules.setAlignment(Pos.TOP_LEFT);
        modules.add(weight, 1,2);
        modules.add(mod_label, 0, 1);
        modules.add(weight_label, 1, 1);
        modules.add(mod1, 0,2);


        grid_buttons.setPadding(new Insets(20, 0, 20, 0));
        grid_buttons.setRight(lineups_box);
        grid_buttons.setTop(textfields);
        grid_buttons.setBottom(buttons);
        grid_buttons.setCenter(imv1);
        grid_buttons.setLeft(modules);


        mma_tab.setContent(grid_buttons);

        tp.getTabs().addAll(mma_tab, golf_tab);
        Scene scene = new Scene(tp, 750, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void handle(ActionEvent event) {
        //System.out.print(event.getSource() + "\n");
        plyr_incl_str = plyr_incl_text.getText();
        plyr_excl_str = plyr_exclude_text.getText();
        url_text_string = url_text.getText();
            System.out.print(weight.getText().length());
            if (weight.getText().length() == 0)
                {
                mod1_weight = 1;
                }
            else{
            try {
                mod1_weight = Integer.parseInt(weight.getText());
            }catch(Exception e) {
                PopUp.display();
                weight.clear();
                return;
            }
            }
        System.out.print(plyr_excl_str +"\n"+ plyr_incl_str +"\n"+ url_text_string +"\n"+mod1_weight);
        if (event.getSource() == run) {
            //Scraper
            scrape.MainScrape scrape = new MainScrape();
            try {
                scrape.scrape(url_text_string);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Module.Module[] moduleArray = new Module.Module[number_of_modules];
            int modCount = 0;
            if(mod1_bool){
                moduleArray[modCount++] = new MMAOddsModule("dfs/res/mma/WebScrape.csv");
            }
            ModuleProcessor modProc = new ModuleProcessor(moduleArray);
            //Calculate Value
            Map<String, Tuple<Integer, Integer>> EVmap = new HashMap<>();
            try {
                EVmap = modProc.calculateModules();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //Generate Lineup
            MMA.Lineup lineup = new MMA.Lineup(EVmap);
            String[] inc = new String[0];
            String[] exc = new String[0];
            if(plyr_incl_str.length() > 0) {
                inc = plyr_incl_str.split("\\s*,\\s*");
            }
            if(plyr_excl_str.length() > 0) {
                exc = plyr_excl_str.split("\\s*,\\s*");
            }

            ArrayList<String[]> list = lineup.genOptimalLineup(EVmap, 50000, 5, inc, exc, number_of_lineups);
            for (String[] l : list) {
                for (String item : l) {
                    //System.out.print(l + "\n" );
                    //output = output.concat(item + ", ");
                    System.out.print(item + ", ");
                }
                System.out.print("\n");
            }

            Results.display(plyr_excl_str, plyr_incl_str, list);

        }


        else if (event.getSource() == cancel) {
            System.exit(0);
        }


    }
}