package Main;

import Golf.GolfOutrightOddsModule;
import Golf.GolfTopTenOddsModule;
import Golf.RankingModule;
import Golf.StartTimeModule;
import MMA.Tuple;
import Module.ModuleProcessor;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import scrape.GolfScrape;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jonathan on 11/9/2015.
 */



public class GUI_Golf{
    public static Button run;
    public static Button cancel;
    public static ToggleButton mod1 = new ToggleButton("module1");
    public static ToggleButton mod2 = new ToggleButton("module2");
    public static ToggleButton mod3 = new ToggleButton("module3");
    public static ToggleButton mod4 = new ToggleButton("module4");
    public static ToggleButton mod5 = new ToggleButton("module5");
    public static ToggleButton mod6 = new ToggleButton("module6");
    public static ToggleButton lineup1 = new ToggleButton("1");
    public static ToggleButton lineup2 = new ToggleButton("5");
    public static ToggleButton lineup3 = new ToggleButton("10");
    public static ToggleButton lineup4 = new ToggleButton("15");
    public static ToggleButton lineup5 = new ToggleButton("20");
    public static ToggleButton lineup6 = new ToggleButton("25");
    public static ToggleGroup lineup_group = new ToggleGroup();
    public static boolean mod1_bool = false;
    public static boolean mod2_bool = false;
    public static boolean mod3_bool = false;
    public static boolean mod4_bool = false;
    public static boolean mod5_bool = false;
    public static boolean mod6_bool = false;
    public static boolean mod7_bool = false;
    public static boolean mod8_bool = false;
    public static boolean mod9_bool = false;
    public static String plyr_incl_str;
    public static String plyr_excl_str;
    public static String url_text_string;
    public static Integer mod1_weight = 1;
    public static Integer mod2_weight = 1;
    public static Integer mod3_weight = 1;
    public static Integer mod4_weight = 1;
    public static Integer mod5_weight = 1;
    public static Integer mod6_weight = 1;
    public static Integer mod7_weight = 1;
    public static Integer mod8_weight = 1;
    public static Integer mod9_weight = 1;

    public static TextField plyr_incl_text;
    public static TextField plyr_exclude_text;
    public static TextField url_text;
    public static TextField weight_mod1;
    public static TextField weight_mod2;
    public static TextField weight_mod3;
    public static TextField weight_mod4;
    public static TextField weight_mod5;
    public static TextField weight_mod6;
    public static TextField weight_mod7;
    public static TextField weight_mod8;
    public static TextField weight_mod9;
    public static Integer number_of_lineups = new Integer(1);

    public static User_Input user_input1 = new User_Input();
    public static User_Input user_input2 = new User_Input();
    public static User_Input user_input3 = new User_Input();
    public static User_Input user_input4 = new User_Input();
    public static User_Input user_input5 = new User_Input();
    public static User_Input userInput6 = new User_Input();
    public static User_Input user_input7 = new User_Input();
    public static User_Input user_input8 = new User_Input();
    public static User_Input user_input9 = new User_Input();

public static Integer start_time;
    private static int number_of_modules = 0;


    public static void display_golf(Tab tab) throws Exception {
        Image image1 = new Image("https://upload.wikimedia.org/wikipedia/en/thumb/7/77/PGA_Tour_logo.svg/801px-PGA_Tour_logo.svg.png");
        ImageView imv1 = new ImageView();


        imv1.setImage(image1);
        imv1.setFitWidth(350);
        imv1.setFitHeight(350);
        imv1.setPreserveRatio(true);
        imv1.setSmooth(true);
        imv1.setCache(true);


        lineup1.setToggleGroup(lineup_group);
        lineup2.setToggleGroup(lineup_group);
        lineup3.setToggleGroup(lineup_group);
        lineup4.setToggleGroup(lineup_group);
        lineup5.setToggleGroup(lineup_group);
        lineup6.setToggleGroup(lineup_group);

        lineup_group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if (newValue == lineup1) {
                    number_of_lineups = 1;
                } else if (newValue == lineup2) {
                    number_of_lineups = 5;
                } else if (newValue == lineup3) {
                    number_of_lineups = 10;
                } else if (newValue == lineup4) {
                    number_of_lineups = 15;
                } else if (newValue == lineup5) {
                    number_of_lineups = 20;
                } else if (newValue == lineup6) {
                    number_of_lineups = 25;
                }
            }
        });
        Label lineups_label = new Label("Please select the number" + "\n" + "of lineups to create:");
        VBox lineups_box = new VBox();
        lineups_box.getChildren().addAll(lineups_label, lineup1, lineup2, lineup3, lineup4, lineup5, lineup6);
        lineups_box.setAlignment(Pos.CENTER);
        lineups_box.setPadding(new Insets(10,25,10,10));
        lineups_box.setSpacing(10);


        BorderPane grid_buttons = new BorderPane();

        //Text Fields
        GridPane textfields = new GridPane();
        textfields.setAlignment(Pos.TOP_CENTER);

        Label url_label = new Label("Please Enter the URL: ");
        Label plyr_include = new Label("Enter players to include: ");
        Label plyr_exclude = new Label("Enter players to exclude: ");

        url_text = new TextField();
        url_text.setPromptText("thegolfaficionado.blogspot.com");
        url_text.setMinWidth(450);

        plyr_incl_text = new TextField();
        plyr_incl_text.setPromptText("e.g. John Doe, Jane Doe");

        plyr_exclude_text = new TextField();
        plyr_exclude_text.setPromptText("e.g. John Doe, Jane Doe");

        HBox url = new HBox();
        url.setSpacing(10);
        url.getChildren().addAll(url_label, url_text);

        HBox exclude_include = new HBox();
        exclude_include.getChildren().addAll(plyr_exclude, plyr_exclude_text, plyr_include, plyr_incl_text);
        exclude_include.setSpacing(10);

        textfields.setVgap(10);
        textfields.setPadding(new Insets(10, 10, 10, 10));
        textfields.add(exclude_include, 0, 1);
        textfields.add(url, 0, 2);



        //Creates the buttons and sets handler
        run = new Button("Run");
        cancel = new Button("Cancel");

        run.setPrefSize(75, 50);
        cancel.setPrefSize(75, 50);


        HBox buttons = new HBox();
        buttons.setSpacing(10);
        buttons.setPadding(new Insets(0, 20, 10, 20));
        buttons.getChildren().addAll(run, cancel);

        buttons.setAlignment(Pos.BOTTOM_RIGHT);

        //Toggle buttons
        GridPane modules = new GridPane();
        Label mod_label = new Label("Please select" + "\n" + " Modules: ");
        Label weight_label = new Label("Please input"+"\n" +" weight: ");
        weight_mod1 = new TextField();
        weight_mod1.setMaxWidth(50);
        weight_mod1.setPromptText("1");
        weight_mod2 = new TextField();
        weight_mod2.setMaxWidth(50);
        weight_mod2.setPromptText("1");
        weight_mod3 = new TextField();
        weight_mod3.setMaxWidth(50);
        weight_mod3.setPromptText("1");
        weight_mod4 = new TextField();
        weight_mod4.setMaxWidth(50);
        weight_mod4.setPromptText("1");
        weight_mod5 = new TextField();
        weight_mod5.setMaxWidth(50);
        weight_mod5.setPromptText("1");
        weight_mod6 = new TextField();
        weight_mod6.setMaxWidth(50);
        weight_mod6.setPromptText("1");
        weight_mod7 = new TextField();
        weight_mod7.setMaxWidth(50);
        weight_mod7.setPromptText("1");
        weight_mod8 = new TextField();
        weight_mod8.setMaxWidth(50);
        weight_mod8.setPromptText("1");
        weight_mod9 = new TextField();
        weight_mod9.setMaxWidth(50);
        weight_mod9.setPromptText("1");

        ToggleButton mod1 = new ToggleButton("Outright");
        ToggleButton mod2 = new ToggleButton("Top Ten");
        ToggleButton mod3 = new ToggleButton("Par 3 Scoring");
        ToggleButton mod4 = new ToggleButton("Par 4 Scoring");
        ToggleButton mod5 = new ToggleButton("Par 5 Scoring");
        ToggleButton mod6 = new ToggleButton("Start Time");
        ToggleButton mod7 = new ToggleButton("Strokes+ Putting");
        ToggleButton mod8 = new ToggleButton("Total Driving");
        ToggleButton mod9 = new ToggleButton("Strokes+ TTG");

        mod1.setMaxWidth(150);
        mod2.setMaxWidth(150);
        mod3.setMaxWidth(150);
        mod4.setMaxWidth(150);
        mod5.setMaxWidth(150);
        mod6.setMaxWidth(150);
        mod7.setMaxWidth(150);
        mod8.setMaxWidth(150);
        mod9.setMaxWidth(150);
        //changes bool when button is selected

        mod1.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (oldValue == false) {
                    URL_Prompt.display(user_input1);
                    System.out.println(user_input1.getUrl());
                    mod1_bool =true;
                    number_of_modules++;
                } else {
                    mod1_bool = false;
                    number_of_modules--;
                }
            }
        });
        mod2.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (oldValue == false) {
                    URL_Prompt.display(user_input2);
                    System.out.println(user_input2.getUrl());


                    mod2_bool = true;
                    number_of_modules++;
                }
                else {
                    mod2_bool = false;
                    number_of_modules--;
                }
            }
        });
        mod3.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (oldValue == false) {
                    URL_Prompt.display(user_input3);
                    System.out.println(user_input3.getUrl());

                    mod3_bool = true;
                    number_of_modules++;

                }
                else {
                    mod3_bool = false;
                    number_of_modules--;
                }
            }
        });
        mod4.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (oldValue == false) {
                    URL_Prompt.display(user_input4);
                    System.out.println(user_input4.getUrl());

                    mod4_bool = true;
                    number_of_modules++;

                }
                else {
                    mod4_bool = false;
                    number_of_modules--;
                }
            }
        });
        mod5.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (oldValue == false) {
                    URL_Prompt.display(user_input5);
                    System.out.println(user_input5.getUrl());

                    mod5_bool = true;
                    number_of_modules++;

                }
                else {
                    mod5_bool = false;
                    number_of_modules--;
                }
            }
        });
        mod6.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (oldValue == false) {
                    start_time = Start_Time_PopUp.display(userInput6);
                    System.out.println(userInput6.getTime());
                    System.out.println(userInput6.getAfter() + "\n");
                    System.out.println(userInput6.getBefore() + "\n");
                    mod6_bool = true;
                    number_of_modules++;
                } else {
                    mod6_bool = false;
                    userInput6.setBefore(false);
                    number_of_modules--;
                }

            }
        });
        mod7.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (oldValue == false) {
                    URL_Prompt.display(user_input7);
                    System.out.println(user_input7.getUrl());

                    mod7_bool = true;
                    number_of_modules++;

                }
                else {
                    mod7_bool = false;
                    number_of_modules--;
                }
            }
        });
        mod8.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (oldValue == false) {
                    URL_Prompt.display(user_input8);
                    System.out.println(user_input8.getUrl());

                    mod8_bool = true;
                    number_of_modules++;
                }
                else {
                    mod8_bool = false;
                    number_of_modules--;
                }
            }
        });
        mod9.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (oldValue == false) {
                    URL_Prompt.display(user_input9);
                    System.out.println(user_input9.getUrl());

                    mod9_bool = true;
                    number_of_modules++;

                }
                else {
                    mod9_bool = false;
                    number_of_modules--;
                }
            }
        });

        //sets layout
        modules.setPadding(new Insets(10, 10, 10, 10));
        modules.setVgap(10);
        modules.setHgap(15);
        modules.setAlignment(Pos.TOP_LEFT);

        modules.add(mod_label, 0, 1);
        modules.add(mod1, 0, 2);
        modules.add(mod2, 0,3);
        modules.add(mod3, 0,4);
        modules.add(mod4, 0,5);
        modules.add(mod5, 0, 6);
        modules.add(mod6, 0, 7);
        modules.add(mod7, 0,8);
        modules.add(mod8, 0, 9);
        modules.add(mod9, 0, 10);

        modules.add(weight_label, 1, 1);
        modules.add(weight_mod1, 1, 2);
        modules.add(weight_mod2, 1, 3);
        modules.add(weight_mod3, 1, 4);
        modules.add(weight_mod4, 1, 5);
        modules.add(weight_mod5, 1, 6);
        modules.add(weight_mod6, 1, 7);
        modules.add(weight_mod7, 1, 8);
        modules.add(weight_mod8, 1, 9);
        modules.add(weight_mod9, 1, 10);


        grid_buttons.setPadding(new Insets(20, 0, 20, 0));
        grid_buttons.setRight(lineups_box);
        grid_buttons.setTop(textfields);
        grid_buttons.setBottom(buttons);
        grid_buttons.setCenter(imv1);
        grid_buttons.setLeft(modules);


        run.setOnAction(e -> {
            plyr_incl_str = plyr_incl_text.getText();
            plyr_excl_str = plyr_exclude_text.getText();
            url_text_string = url_text.getText();
            System.out.print(weight_mod1.getText().length());
            if (weight_mod1.getText().isEmpty()) {
                mod1_weight = 1;
            } else {
                try {
                    mod1_weight = Integer.parseInt(weight_mod1.getText());
                } catch (Exception ex) {
                    PopUp.display();
                    weight_mod1.clear();
                    return;
                }
            }
            if (weight_mod2.getText().length() == 0) {
                mod2_weight = 1;
            } else {
                try {
                    mod2_weight = Integer.parseInt(weight_mod2.getText());
                } catch (Exception ex) {
                    PopUp.display();
                    weight_mod2.clear();
                    return;
                }
            }
            if (weight_mod3.getText().length() == 0) {
                mod3_weight = 1;
            } else {
                try {
                    mod3_weight = Integer.parseInt(weight_mod3.getText());
                } catch (Exception ex1) {
                    PopUp.display();
                    weight_mod3.clear();
                    return;
                }
            }
            if (weight_mod4.getText().length() == 0) {
                mod4_weight = 1;
            } else {
                try {
                    mod4_weight = Integer.parseInt(weight_mod4.getText());
                } catch (Exception ex1) {
                    PopUp.display();
                    weight_mod4.clear();
                    return;
                }
            }
            if (weight_mod5.getText().length() == 0) {
                mod5_weight = 1;
            } else {
                try {
                    mod5_weight = Integer.parseInt(weight_mod5.getText());
                } catch (Exception ex1) {
                    PopUp.display();
                    weight_mod5.clear();
                    return;
                }
            }
            if (weight_mod6.getText().length() == 0) {
                mod6_weight = 1;
            } else {
                try {
                    mod6_weight = Integer.parseInt(weight_mod6.getText());
                } catch (Exception ex) {
                    PopUp.display();
                    weight_mod6.clear();
                    return;
                }


            }
            if (weight_mod7.getText().length() == 0) {
                mod6_weight = 1;
            } else {
                try {
                    mod7_weight = Integer.parseInt(weight_mod7.getText());
                } catch (Exception ex) {
                    PopUp.display();
                    weight_mod7.clear();
                    return;
                }


            }
            if (weight_mod8.getText().length() == 0) {
                mod8_weight = 1;
            } else {
                try {
                    mod8_weight = Integer.parseInt(weight_mod8.getText());
                } catch (Exception ex) {
                    PopUp.display();
                    weight_mod8.clear();
                    return;
                }


            }
            if (weight_mod9.getText().length() == 0) {
                mod6_weight = 1;
            } else {
                try {
                    mod9_weight = Integer.parseInt(weight_mod9.getText());
                } catch (Exception ex) {
                    PopUp.display();
                    weight_mod9.clear();
                    return;
                }


            }
            System.out.println("include: " + plyr_incl_str + "\n");
            System.out.println("exclude: " + plyr_excl_str + "\n");
            System.out.println("url: " + url_text_string + "\n");
            System.out.println(mod1_weight + "\n");
            System.out.println(mod2_weight + "\n");
            System.out.println(mod3_weight + "\n");
            System.out.println(mod4_weight + "\n");
            System.out.println(mod5_weight + "\n");
            System.out.println(mod6_weight + "\n");


            //Scraper
            GolfScrape scrape = new GolfScrape();
            try {
                if(mod1_bool) {
                    scrape.scrapeWebsite(user_input1.getUrl());
                }
                if(mod2_bool) {
                    scrape.scrapeWebsite(user_input2.getUrl());
                }
                if(mod3_bool) {
                    scrape.scrapeWebsite(user_input3.getUrl());
                }
                if(mod4_bool) {
                    scrape.scrapeWebsite(user_input4.getUrl());
                }
                if(mod5_bool) {
                    scrape.scrapeWebsite(user_input5.getUrl());
                }
                if(mod6_bool) {
                    scrape.scrapeWebsite(userInput6.getUrl());
                }
                if(mod7_bool) {
                    scrape.scrapeWebsite(user_input7.getUrl());
                }
                if(mod8_bool) {
                    scrape.scrapeWebsite(user_input8.getUrl());
                }
                if(mod9_bool) {
                    scrape.scrapeWebsite(user_input9.getUrl());
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            Module.Module[] moduleArray = new Module.Module[number_of_modules];
            int modCount = 0;
            if(mod1_bool){
                moduleArray[modCount++] = new GolfOutrightOddsModule("dfs/res/golf/PaddyOutright.csv", mod1_weight);
            }
            if(mod2_bool){
                moduleArray[modCount++] = new GolfTopTenOddsModule("dfs/res/golf/PaddyTopTen.csv", mod2_weight);
            }
            if(mod3_bool){
                moduleArray[modCount++] = new RankingModule("dfs/res/golf/Par3.csv", mod3_weight);
            }
            if(mod4_bool){
                moduleArray[modCount++] = new RankingModule("dfs/res/golf/Par4.csv", mod4_weight);
            }
            if(mod5_bool){
                moduleArray[modCount++] = new RankingModule("dfs/res/golf/Par5.csv", mod5_weight);
            }
            if(mod6_bool){
                moduleArray[modCount++] = new StartTimeModule("dfs/res/golf/StartTime.csv", mod6_weight, userInput6.getTime(),userInput6.getBefore());
            }
            if(mod7_bool){
                moduleArray[modCount++] = new RankingModule("dfs/res/golf/PuttingStrokesGained.csv", mod7_weight);
            }
            if(mod8_bool){
                moduleArray[modCount++] = new RankingModule("dfs/res/golf/TotalDriving.csv", mod8_weight);
            }
            if(mod9_bool){
                moduleArray[modCount++] = new RankingModule("dfs/res/golf/StrokesGainedTTG.csv", mod9_weight);
            }
            ModuleProcessor modProc = new ModuleProcessor(moduleArray);
            //Calculate Value
            Map<String, Tuple<Integer, Integer>> EVmap = new HashMap<>();
            try {
                EVmap = modProc.calculateModules();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            //Generate Lineup
            MMA.Lineup lineup = new MMA.Lineup(EVmap);
            String[] inc = new String[0];
            String[] exc = new String[0];
            if(plyr_incl_str.length() > 0) {
                inc = plyr_incl_str.split("\\s*,\\s*");
            }
            if(plyr_excl_str.length() > 0) {
                exc = plyr_excl_str.split("\\s*,\\s*");
            }

            ArrayList<String[]> list = lineup.genOptimalLineup(EVmap, 50000, 6, inc, exc, number_of_lineups);
            for (String[] l : list) {
                for (String item : l) {
                    //System.out.print(l + "\n" );
                    //output = output.concat(item + ", ");
                    System.out.print(item + ", ");
                }
                System.out.print("\n");
            }
            Results.display(plyr_excl_str, plyr_incl_str, list);
        });





        cancel.setOnAction(e -> {
            System.exit(0);
        });




        tab.setContent(grid_buttons);



    }
}
