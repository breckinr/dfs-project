package Main;

import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.awt.*;

/**
 * Created by Jonathan on 12/1/2015.
 */
public class PopUp {

    public static void display()
    {
        Paint color = Color.RED;
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Error");
        window.setMinWidth(200);
        window.setMinHeight(200);
        BorderPane pane = new BorderPane();
        Text error_message = new Text("Please enter a numeric value" + "\n" + "for weight");
        error_message.setFont(Font.font("Times New Roman", FontPosture.ITALIC, 18));
        pane.setCenter(error_message);
        Scene scene = new Scene(pane);
        window.setScene(scene);
        window.showAndWait();

    }


}
