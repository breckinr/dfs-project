package Main;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Jonathan on 11/9/2015.
 */
public class Results {
    public static void display(String exclude, String include, ArrayList<String[]> items){
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Results");
        window.setMinWidth(300);
        window.setMinHeight(200);
        Label label = new Label();
        label.setText("Include: " + include + "\n" + "Exclude: " + exclude);
        Label Lineups = new Label();
        String lineupString = "";
        for(String[] item : items) {
            lineupString += Arrays.toString(item) + "\n";
        }
        Lineups.setText("Lineups: \n" + lineupString);
        Button okay = new Button("okay");
        okay.setMinHeight(50);
        okay.setMinWidth(50);
        okay.setOnAction(e -> window.close());

        HBox lineups = new HBox();
        VBox data = new VBox();
        HBox button = new HBox();
        lineups.getChildren().addAll(Lineups);
        data.getChildren().addAll(label);
        button.getChildren().addAll(okay);
        lineups.setAlignment(Pos.CENTER);
        data.setAlignment(Pos.CENTER);
        button.setAlignment(Pos.BOTTOM_RIGHT);

        BorderPane pane = new BorderPane();
        pane.setBottom(button);
        pane.setTop(data);
        pane.setCenter(lineups);

        Scene scene = new Scene(pane);
        window.setScene(scene);
        window.showAndWait();



    }







}
