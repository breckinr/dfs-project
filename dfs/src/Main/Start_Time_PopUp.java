package Main;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;


/**
 * Created by Jonathan on 12/3/2015.
 */

public class Start_Time_PopUp {
    public static ToggleButton before = new ToggleButton("Before");
    public static ToggleButton after = new ToggleButton("After");
    public static Integer start_time = 2400;
    public static Integer display(User_Input userInput)

    {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Start Time Data");
        window.setMinWidth(150);
        window.setMinHeight(150);
        BorderPane main_pane = new BorderPane();
        GridPane pane = new GridPane();
        TextField time = new TextField();
        time.setMinWidth(150);
        Label time_label = new Label("Please enter Time in Military: ");
        time.setPromptText("e.g. 1500");
        HBox okay_box = new HBox();
        Button okay = new Button("OK");
        okay.setMinWidth(75);
        okay.setMinHeight(20);
        okay_box.getChildren().addAll(okay);
        okay_box.setAlignment(Pos.BOTTOM_RIGHT);
        okay_box.setPadding(new Insets(10, 10, 10, 10));
        ToggleGroup before_after_group = new ToggleGroup();
        Label before_after = new Label("Choose one: ");
        before.setMinWidth(100);
        after.setMinWidth(100);
        before.setToggleGroup(before_after_group);
        after.setToggleGroup(before_after_group);
        okay.setOnAction(event -> {
            if (before_after_group.getSelectedToggle() == before) {
                userInput.setBefore(true);
            }
            if (before_after_group.getSelectedToggle() == after) {
                userInput.setAfter(true);
            }
            try {
                start_time = Integer.parseInt(time.getText());
                if (0 > start_time || start_time > 2359) {
                    throw new NumberFormatException();
                }
            } catch (Exception ex) {
                Time_Error_PopUp.display();
                start_time = Start_Time_PopUp.display(userInput);
            }
            userInput.setTime(start_time);
            window.close();
        });

        pane.setPadding(new Insets(10, 10, 10, 10));
        pane.setAlignment(Pos.CENTER);
        pane.setVgap(10);
        pane.setHgap(10);

        pane.add(time_label, 0, 1);
        pane.add(time, 1, 1);
        pane.add(before_after, 0, 3);
        pane.add(before, 1, 3);
        pane.add(after, 2, 3);

        main_pane.setBottom(okay_box);
        main_pane.setCenter(pane);
        Scene scene = new Scene(main_pane);
        window.setScene(scene);
        window.showAndWait();

         return start_time;

    }

    }

