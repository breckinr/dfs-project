package Main;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by Jonathan on 12/4/2015.
 */
public class Time_Error_PopUp
{
    public static void display()
    {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Time Error");
        window.setMinWidth(200);
        window.setMinHeight(200);
        BorderPane pane = new BorderPane();
        Text error_message = new Text("Please enter a valid time");
        error_message.setFont(Font.font("Times New Roman", FontPosture.ITALIC, 18));
        pane.setCenter(error_message);
        Scene scene = new Scene(pane);
        window.setScene(scene);
        window.showAndWait();
    }
}
