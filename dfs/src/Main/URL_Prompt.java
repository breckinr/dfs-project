package Main;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by Jonathan on 12/5/2015.
 */
public class URL_Prompt {
    public static Button okay;
    public static String url = "";
    public static String display(User_Input userInput){

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("URL Prompt");
        window.setMinWidth(500);
        window.setMinHeight(100);
        BorderPane borderPane = new BorderPane();
        GridPane pane = new GridPane();
        Label url_label = new Label("Please enter a URL to use: ");
        TextField url_text = new TextField();
        url_text.setPromptText("thegolfaficionado.blogspot.com");
        url_text.setMinWidth(300);
        okay = new Button("OK");
        HBox okay_box = new HBox();
        okay_box.setAlignment(Pos.BOTTOM_RIGHT);
        okay_box.setPadding(new Insets(10, 10, 10, 10));
        okay_box.getChildren().addAll(okay);
        okay.setOnAction(event1 -> {
            url = url_text.getText();
            try {
                if (url.length() == 0) {
                    throw new NullPointerException();
                }
            } catch (Exception ex_1) {
                URL_Error_PopUp.display();
                url = URL_Prompt.display(userInput);
            }
                userInput.setUrl(url);
                System.out.print(userInput.getUrl());
                window.close();
        });

        pane.setAlignment(Pos.CENTER);
        pane.setVgap(10);
        pane.setHgap(10);
        pane.add(url_label, 0, 1);
        pane.add(url_text, 0, 2);
        borderPane.setCenter(pane);
        borderPane.setBottom(okay_box);
        Scene scene = new Scene(borderPane);
        window.setScene(scene);
        window.show();

        return userInput.getUrl();

    }



}
