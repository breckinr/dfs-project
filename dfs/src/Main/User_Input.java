package Main;

/**
 * Created by Jonathan on 12/5/2015.
 */
public class User_Input {
    private static boolean before, after;
    private static String url;
    private static Integer time;
    public User_Input() {
        this.time = -1;
        this.before = false;
        this.after = false;
        this.url = "";
    }
    public User_Input(String str, boolean before, boolean after, Integer time)
    {
        this.time = time;
        this.url = str;
        this.before = before;
        this.after = after;
    }
    public User_Input(String url)
    {
        this.url = url;
        this.time = 0;
        this.before = false;
        this.after =false;
    }
    public Integer getTime() {return time;}
    public boolean getBefore(){return before;}
    public boolean getAfter(){return after;}
    public String getUrl(){return url;}
    public void setBefore(boolean before){User_Input.before = before;}
    public void setAfter(boolean after){User_Input.after = after;}
    public void setUrl(String url){User_Input.url = url;}
    public static void setTime(Integer time) {User_Input.time = time;}
}

