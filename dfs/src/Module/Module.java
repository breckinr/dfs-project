package Module;

import MMA.Tuple;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by breckinr on 12/2/15.
 */
public interface Module {


    //called on first module processed, returns a new Map
    public Map<String, Tuple<Integer, Integer>> getResults();
    //called on all modules processed after first, modifies the map passed in
    public void getResults(Map<String, Tuple<Integer, Integer>> oldMap);


}
