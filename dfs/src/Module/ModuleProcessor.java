package Module;

import MMA.Tuple;

import java.util.Map;

/**
 * Created by breckinr on 12/2/15.
 */
public class ModuleProcessor{
    Module[] modules;
    public ModuleProcessor(Module[] modules){
            if(modules.length == 0) {
                System.err.println("No modules selected");
                System.exit(1);
            }
        this.modules = modules;
    }

    public Map<String, Tuple<Integer, Integer>>  calculateModules(){//Tuple holds points, salary
        Map<String, Tuple<Integer, Integer>> retMap = null;
        int modCount;
        for(modCount = 0; modCount < modules.length; ++ modCount){
            if(modCount == 0) {
                //create initial map
                retMap = modules[0].getResults();
            }
            else {
                //add combined results to existing map
                modules[modCount].getResults(retMap);
            }
        }
        return retMap;
    }


}
