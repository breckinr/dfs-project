package scrape;
import Golf.GolfBean;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class GolfScrape {
    private  Map<String, Integer> golfSalaries;

    public GolfScrape() {
        golfSalaries = new TreeMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader("./dfs/res/golf/DKSalaries.csv"))) {
            String line;
            boolean first = true;
            while ((line = br.readLine()) != null) {
                if (first) {
                    first = false;
                    continue;
                }
                String info[] = line.split(",");
                golfSalaries.put(info[1].toString(), Integer.parseInt(info[2]));
            }
        }
        catch (IOException e) {}
    }

    public void scrapeWebsite(String url) throws IOException {
        if (url.contains("paddypower") || url.contains("googledrive")) {
            scrapePaddy((url));
        }
        if (url.contains("pgatour")) {
            scrapePGA(url);
        }
    }

    void scrapePGA(String url) throws IOException {
        String category = new String();
        if (url.contains("stat.171")) {
            category = "Par3";
        } else if (url.contains("stat.172")) {
            category = "Par4";
        } else if (url.contains("stat.173")) {
            category = "Par5";
        } else if (url.contains("stat.02675")) {
            category = "StrokesGainedTTG";
        }
        else if (url.contains("stat.129")) {
            category = "TotalDriving";
        }
        else if (url.contains("stat.02564")) {
            category = "PuttingStrokesGained";
        }
        if (category.isEmpty()) {
            System.out.println("Error in parsing PGA");
            System.exit(0);
        }
        Document doc = Jsoup.connect(url).get();
        Elements table = doc.select("div[class=details-table-wrap]").first().select("tr[id]");
        scrapePar(table, category);
    }

    void scrapePar(Elements table, String category) {
        ArrayList<GolfBean> golfList = new ArrayList<>();
        for (Element player : table) {
            Elements playerData = player.select("td");
            ArrayList<String> stats = new ArrayList<>();
            for (Element data : playerData) {
                if (data.text().toString().charAt(0) == 'T' &&
                        Character.isDigit(data.text().toString().charAt(1))) {
                    stats.add(data.text().toString().substring(1));
                } else if (data.text().toString().charAt(0) == 'E' &&
                        data.text().toString().length() == 1) {
                    stats.add("0");
                } else {
                    stats.add(data.text().toString());
                }
            }
            assert stats.size() == 7;
            /*
            bean.setPrevRank(Integer.parseInt(stats.get(0)));
            bean.setRank(Integer.parseInt(stats.get(1)));
            bean.setName(stats.get(2));
            bean.setRounds(Integer.parseInt(stats.get(3)));
            bean.setStatus(Integer.parseInt(stats.get(4)));
            bean.setAverage(Double.parseDouble(stats.get(5)));
            bean.setTotalPar(Integer.parseInt(stats.get(6)));
            */
            GolfBean bean = new GolfBean();
            bean.setRank(Integer.parseInt(stats.get(0)));
            bean.setName(stats.get(2).replace(' ', ' '));

            if (golfSalaries.containsKey(bean.getName())) {
                bean.setSalary(golfSalaries.get(bean.getName()));
            }

            golfList.add(bean);
        }
        ArrayList<GolfBean> removedList = new ArrayList<>();
        for (GolfBean bean : golfList) {
            if (bean.getSalary() == 0) {
                removedList.add(bean);
            }
        }
        golfList.removeAll(removedList);
        try {
            writeWithCsvBeanWriter(golfList, category);
        } catch (Exception e) {
            System.out.println("WARN: Golf writer exception: " + category);
        }
    }

    void scrapePaddy(String url) throws IOException {
        Document doc = Jsoup.connect(url).get();
        //Element event = doc.select("table[class=mkt-hdr]").first().nextElementSibling();
        //Elements links = event.select("a");
        //eventUrl = link.attr("href"); old code
        String category = "Paddy";
/*
        if (links.isEmpty()){
            eventUrl = url;
            links.add(doc.select("title").first());
        }
        System.out.println("link txt " + links.first().text().toString() );
*/
        //for (Element link : links) {
        Element link = doc.select("title").first();
        if (link.text().contains("Top 10")) {
            category += "Top10";
        }
        else if (link.text().contains("Outright Betting")) {
            category += "Outright";
        }
        //}

        doc = Jsoup.connect(url).get();
        Elements golfers = doc.select("table[class=mkt] > tbody > tr > td[class] > div[class]");
        ArrayList<GolfBean> golfList = new ArrayList<>();
        for (Element golfer : golfers) {
            String[] info = golfer.text().toString().split(" ");
            String fullName = "";
            for (String name : info) {
                if (!name.contains("/")) {
                    fullName += name + " ";
                }
            }

            double numerator = 1.0, denominator = 1.0;
            if (!info[info.length - 1].equals("evens")) {
                numerator = Double.parseDouble(info[info.length - 1].split("/")[0]);
                denominator = Double.parseDouble(info[info.length - 1].split("/")[1]);
            }

            GolfBean bean = new GolfBean();
            bean.setName(fullName.substring(0, fullName.length() - 1).replace(' ', ' '));
            String name[] = bean.getName().split(",");
            bean.setName(name[1].substring(1) + " " + name[0]);
            bean.setWin(numerator / denominator);

            if (golfSalaries.containsKey(bean.getName())) {
                bean.setSalary(golfSalaries.get(bean.getName()));
            }

            golfList.add(bean);
        }

        ArrayList<GolfBean> removedList = new ArrayList<>();
        for (GolfBean bean : golfList) {
            if (bean.getSalary() == 0) {
                removedList.add(bean);
            }
        }
        golfList.removeAll(removedList);

        try {
            writeWithCsvBeanWriter(golfList, category);
        } catch (Exception e) {
            System.out.println("WARN: Golf writer exception");
        }
    }

    private static void writeWithCsvBeanWriter(final ArrayList<GolfBean> beans, String category) throws Exception {
        ICsvBeanWriter beanWriter = null;
        GolfBean golferBean = new GolfBean();

        try {
            beanWriter = new CsvBeanWriter(new FileWriter("dfs/res/golf/" + category + ".csv"),
                    CsvPreference.STANDARD_PREFERENCE);

            final String[] header = new String[]{"Rank", "Name", "Win", "Salary"};
            final CellProcessor[] processors = golferBean.getProcessors();

            // write the header
            beanWriter.writeHeader(header);

            // write the beans
            for (GolfBean golfer : beans) {
                beanWriter.write(golfer, header, processors);
            }

        } finally {
            if (beanWriter != null) {
                beanWriter.close();
            }
        }
    }

}