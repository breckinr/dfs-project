package scrape;
import MMA.FighterBean;
import scrape.GolfScrape;
import MMA.ParseMoneyline;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.constraint.UniqueHashCode;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

public class MainScrape {
    private Map<String, Integer> mmaSalaries;
    public static void main(String[] args) throws IOException {
        //MainScrape temp = new MainScrape();
        //temp.scrape(); // MMA
        ArrayList<String> websites = new ArrayList<>();
        websites.add("http://www.pgatour.com/stats/stat.171.html");
        websites.add("http://www.pgatour.com/stats/stat.172.html");
        websites.add("http://www.pgatour.com/stats/stat.173.html");
        websites.add("http://www.pgatour.com/stats/stat.02675.html");
        websites.add("http://www.googledrive.com/host/0B9c_zZJTC2QzZF9QWlFsVlVwSTQ");
        websites.add("http://www.googledrive.com/host/0B9c_zZJTC2QzQ3JvTk5zMER0elE");
        websites.add("http://www.pgatour.com/stats/stat.02564.html");
        websites.add("http://www.pgatour.com/stats/stat.129.html");
        GolfScrape g = new GolfScrape();
        for (String url : websites) {
            long startTime = System.nanoTime();
            g.scrapeWebsite(url);
            long end = System.nanoTime();
            System.out.println("time " + (end-startTime)/1000000 );
        }

    }

    public Map<String, Integer> getSalaries() {
        Map<String, Integer> salaries = new TreeMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader("./dfs/res/golf/DKSalaries.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        }
        catch (IOException e) {}
        return salaries;
    }

    public void scrape(String url) throws IOException {
        Document doc = Jsoup.connect(url).get();
        Elements categories = doc.select("tr > th");
        FighterBean fb1 = new FighterBean();
        FighterBean fb2 = new FighterBean();
        boolean next = false;
        boolean seenName = false;
        ArrayList<FighterBean> fbList = new ArrayList<FighterBean>();
        mmaSalaries = new TreeMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader("dfs/res/mma/DKSalaries.csv"))) {
            String line;
            boolean first = true;
            while ((line = br.readLine()) != null) {
                if (first) {
                    first = false;
                    continue;
                }
                String info[] = line.split(",");
                mmaSalaries.put(info[1].toString(), Integer.parseInt(info[2]));
            }
        }
        for (Element category : categories) {
            Elements values = null;
            if (category.nextElementSibling() != null)
                values = category.nextElementSibling().select("a > span > span");
            if (values != null) {
                for (Element value : values) {
                    if (value != null)
                        try {
                            String cat = category.text();
                            //System.out.println(cat);

                            if (isPlayerName(cat) && !fb1.getName().isEmpty() && !fb2.getName().isEmpty()) {
                                //System.out.println("Player name: " + cat);
                                if (!fb1.getName().equals(cat) && !fb2.getName().equals(cat)) {
                                    fb1.setGameInfo("Default");
                                    fb2.setGameInfo("Default");
                                    fbList.add(new FighterBean(fb1));
                                    fbList.add(new FighterBean(fb2));
                                    fb1 = new FighterBean();
                                    fb2 = new FighterBean();
                                    next = false;
                                }
                            }
                            if (mmaSalaries.containsKey(fb1.getName())) {
                                fb1.setSalary(mmaSalaries.get(fb1.getName()));
                            }

                            if (mmaSalaries.containsKey(fb2.getName())) {
                                fb2.setSalary(mmaSalaries.get(fb2.getName()));
                            }
                            Double result = Double.parseDouble(value.text());
                            String tokens[] = cat.split(" ");
                            if (tokens.length == 2) {
                                if (!next && fb1.getName().isEmpty()){
                                    fb1.setName(cat);
                                    fb1.setTeamAbbrev(tokens[tokens.length - 1]);
                                }
                                else if (!next && fb2.getName().isEmpty() && !fb1.getName().equals(cat)) {
                                    fb2.setName(cat);
                                    fb2.setTeamAbbrev(tokens[tokens.length - 1]);
                                    fb2.setOpponent(fb1.getName());
                                    fb1.setOpponent(fb2.getName());
                                    next = true;
                                }
                            }

                            if (cat.contains("wins in round 1")){
                                if (cat.contains(fb1.getTeamAbbrev())) {
                                    fb1.setWr1(result);
                                }
                                else {
                                    fb2.setWr1(result);
                                }
                            }

                            if (cat.contains("wins in round 2")){
                                if (cat.contains(fb1.getTeamAbbrev())) {
                                    fb1.setWr2(result);
                                }
                                else {
                                    fb2.setWr2(result);
                                }
                            }

                            if (cat.contains("wins in round 3")){
                                if (cat.contains(fb1.getTeamAbbrev()) && !fb1.getTeamAbbrev().isEmpty()) {
                                    fb1.setWr3(result);
                                    fb1.setGameInfo("Default");
                                    fbList.add(new FighterBean(fb1));
                                    fb1 = new FighterBean();
                                }
                                else {
                                    fb2.setWr3(result);
                                    fb2.setGameInfo("Default");
                                    fbList.add(new FighterBean(fb2));
                                    fb2 = new FighterBean();
                                }
                                next = false;
                            }

                            if (cat.contains("wins by decision")){
                                if (cat.contains(fb1.getTeamAbbrev())) {
                                    fb1.setWbd(result);
                                }
                                else {
                                    fb2.setWbd(result);
                                }
                            }

                        } catch (NumberFormatException e){ }
                }
            }
        }
        try {
            //System.out.println("size: " + fbList.size());
            writeWithCsvBeanWriter(fbList);
        }
        catch (Exception e) { }
    }
    private boolean isPlayerName(String input) {
        List<String> required = Arrays.asList("rounds", "decision", "draw", "distance", "TKO/KO",
                "submission", "unanimous", "split/majority", "scorecards", "round", "result");
        for (String s : required){
            if (input.contains(s)){
                return false;
            }
        }
        return true;
    }

    private static CellProcessor[] getProcessors() {
        final CellProcessor[] processors = new CellProcessor[]{
                new NotNull(), // Name
                new ParseInt(), // Salary (must be int)
                new NotNull(), // Opponent
                new NotNull(),//GameInfo
                new UniqueHashCode(),//teamAbbrev
                new ParseDouble(), //WR1: chance this fighter will win in rd 1
                new ParseDouble(), //WR2: chance this fighter will win in rd 2
                new ParseDouble(), //WR3: chance this fighter will win in rd 3
                new ParseDouble(), //WBD: chance this fighter will win by decision
        };
        return processors;
    }
    private static void writeWithCsvBeanWriter(final ArrayList<FighterBean> beans) throws Exception {
        ICsvBeanWriter beanWriter = null;
        try {
            beanWriter = new CsvBeanWriter(new FileWriter("dfs/res/mma/WebScrape.csv"),
                    CsvPreference.STANDARD_PREFERENCE);

            final String[] header = new String[]{"Name", "Salary", "Opponent", "GameInfo", "teamAbbrev", "WR1","WR2","WR3","WBD"};
            final CellProcessor[] processors = getProcessors();

            // write the header
            beanWriter.writeHeader(header);

            // write the beans
            for (FighterBean b : beans) {
                beanWriter.write(b, header, processors);
            }

        }
        finally {
            if( beanWriter != null ) {
                beanWriter.close();
            }
        }
    }
}
